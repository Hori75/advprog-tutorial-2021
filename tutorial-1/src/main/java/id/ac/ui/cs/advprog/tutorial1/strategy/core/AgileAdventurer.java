package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AgileAdventurer extends Adventurer {

    private String alias;

    public AgileAdventurer() {
        this.alias = "Agile";
        setAttackBehavior(new AttackWithGun());
        setDefenseBehavior(new DefendWithBarrier());
    }

    public String getAlias() {
        return alias;
    }
}
