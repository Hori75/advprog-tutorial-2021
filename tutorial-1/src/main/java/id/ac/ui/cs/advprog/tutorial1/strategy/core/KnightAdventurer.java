package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class KnightAdventurer extends Adventurer {

    private String alias;

    public KnightAdventurer() {
        this.alias = "Knight";
        setAttackBehavior(new AttackWithSword());
        setDefenseBehavior(new DefendWithArmor());
    }

    public String getAlias() {
        return alias;
    }
}
