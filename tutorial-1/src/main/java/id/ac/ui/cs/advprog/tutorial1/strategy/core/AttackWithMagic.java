package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class AttackWithMagic implements AttackBehavior {
    public String attack() {
        return "Not so long spell chanting";
    }

    public String getType() {
        return "Magic";
    }
}
