package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class MysticAdventurer extends Adventurer {

    private String alias;

    public MysticAdventurer() {
        this.alias = "Mystic";
        setAttackBehavior(new AttackWithMagic());
        setDefenseBehavior(new DefendWithShield());
    }

    public String getAlias() {
        return alias;
    }
}
