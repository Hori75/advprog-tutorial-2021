package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class MysticAdventurer extends Adventurer {


    public MysticAdventurer(Guild guild) {
        this.name = "Mystic";
        this.guild = guild;
    }

    public void update() {
        Quest quest = guild.getQuest();
        switch(guild.getQuestType()) {
            case "D":
                this.getQuests().add(quest);
                break;
            case "E":
                this.getQuests().add(quest);
                break;
            default:
                break;
        }
    }
}
