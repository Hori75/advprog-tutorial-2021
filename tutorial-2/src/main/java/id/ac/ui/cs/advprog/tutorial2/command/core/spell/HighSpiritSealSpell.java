package id.ac.ui.cs.advprog.tutorial2.command.core.spell;

import id.ac.ui.cs.advprog.tutorial2.command.core.spirit.HighSpirit;

public class HighSpiritSealSpell extends HighSpiritSpell {
    
    public HighSpiritSealSpell(HighSpirit spirit) {
        super(spirit);
    }

    public void cast() {
        getSpirit().seal();
    }

    @Override
    public String spellName() {
        return spirit.getRace() + ":Seal";
    }
}
