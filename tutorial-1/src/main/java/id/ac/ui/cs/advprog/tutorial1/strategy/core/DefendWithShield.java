package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithShield implements DefenseBehavior {
    public String defend() {
        return "Attack Repulsor";
    }

    public String getType() {
        return "Shield";
    }
}
