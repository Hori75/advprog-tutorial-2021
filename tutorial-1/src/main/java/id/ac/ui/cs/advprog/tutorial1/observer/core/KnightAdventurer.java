package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class KnightAdventurer extends Adventurer {


    public KnightAdventurer(Guild guild) {
        this.name = "Knight";
        this.guild = guild;
    }

    public void update() {
        Quest quest = guild.getQuest();
        switch(quest.getType()) {
            case "D":
                this.getQuests().add(quest);
                break;
            case "R":
                this.getQuests().add(quest);
                break;
            case "E":
                this.getQuests().add(quest);
                break;
            default:
                break;
        }
    }
}
