package id.ac.ui.cs.advprog.tutorial1.strategy.core;

public class DefendWithArmor implements DefenseBehavior {
    public String defend() {
        return "Armor Deflection";
    }

    public String getType() {
        return "Armor";
    }
}
