package id.ac.ui.cs.advprog.tutorial1.observer.core;

public class AgileAdventurer extends Adventurer {


    public AgileAdventurer(Guild guild) {
        this.name = "Agile";
        this.guild = guild;
    }

    public void update() {
        Quest quest = guild.getQuest();
        switch(guild.getQuestType()) {
            case "D":
                this.getQuests().add(quest);
                break;
            case "R":
                this.getQuests().add(quest);
                break;
            default:
                break;
        }
    }
}
